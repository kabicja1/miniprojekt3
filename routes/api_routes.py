from flask import Blueprint, jsonify, request
from dateutil import parser


api_routes_bp = Blueprint('api_routes', __name__)

def parse_datetime_str(timestamp_str):
    try:
        parsed_datetime = parser.parse(timestamp_str)
        return parsed_datetime
    except ValueError as e:
        print(f"Chyba při parsování řetězce: {e}")
        return None

@api_routes_bp.route('/api/tasks', methods=['GET'])
def get_tasks():
    from app import Measurement
    measurements = Measurement.query.order_by(Measurement.id.desc()).all()
    result = [{"id": measurement.id, "timestamp": measurement.timestamp.strftime("%d/%b/%Y %H:%M:%S"), "temp": measurement.temp} for measurement in measurements]
    return jsonify(result)

@api_routes_bp.route('/api/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    from app import Measurement
    measurements = Measurement.query.order_by(Measurement.id.desc()).limit(task_id).all()
    if measurements:
        result = [{"id": measurement.id, "timestamp": measurement.timestamp.strftime("%d/%b/%Y %H:%M:%S"), "temp": measurement.temp} for measurement in measurements]
        return jsonify(result)
    else:
        return jsonify({"message": "Task not found"}), 404


@api_routes_bp.route('/api/tasks', methods=['POST'])
def create_task():

    from app import Measurement, db, generate_task_id, datetime
    try:
        new_task = request.json
        new_task_id = generate_task_id()
        timestamp_str = new_task['timestamp']
        timestamp = parse_datetime_str(timestamp_str)
        new_measurement = Measurement(id=new_task_id, timestamp=timestamp, temp=new_task['temp'])
        db.session.add(new_measurement)
        db.session.commit()

        return jsonify(new_task), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500

@api_routes_bp.route('/api/delete_last/<int:num_to_delete>', methods=['DELETE'])
def delete_last(num_to_delete):
    from app import Measurement, db
    if num_to_delete > 0:
        # Získání posledních naměřených hodnot z databáze, seřazených od největšího k nejmenšímu ID
        last_measurements = Measurement.query.order_by(Measurement.id.asc()).limit(num_to_delete).all()
        
        if last_measurements:
            # Smazání posledních naměřených hodnot z databáze
            for measurement in last_measurements:
                db.session.delete(measurement)
            db.session.commit()

            # Získání zbývajících naměřených hodnot pro zobrazení
            remaining_measurements = Measurement.query.all()
            # Seřazení zbývajících měření od největšího k nejmenšímu ID
            remaining_measurements_sorted = sorted(remaining_measurements, key=lambda x: x.id, reverse=True)
            result = [{"id": measurement.id, "timestamp": measurement.timestamp.strftime("%d/%b/%Y %H:%M:%S"), "temp": measurement.temp} for measurement in remaining_measurements_sorted]
            return jsonify(result)
        else:
            return jsonify({"message": "No data found"}), 404
    else:
        return jsonify({"message": "Invalid number of items to delete"}), 400


